# Import the logger module:
import myLogger

# Import other init routine module:
import myInit

# Import operating system module:
import os

# Import high level file operations module:
import shutil

# Define the archive location:
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

def restoreArchiveToDirectory(restoreToPath):
    try:
        myLogger.logger.info("\tStarting archive restore:")
        
        # Load the contents of the index.txt file back
        # into the dictionary:
        fileList = {}
        if os.path.isfile(MY_INDEX_FILE):
            indexFile = open(MY_INDEX_FILE,'r')
            contents = indexFile.read()
            if len(contents) > 0:
                fileList = eval(contents)
            
            indexFile.close()

            # Restore each file in the archive:
            for key in fileList:
                drive, pathOnly = os.path.splitdrive(key)
                path, file = os.path.split(pathOnly)
                
                # Restore this file:
                
                # Create the file directory if it does not exist:
                if not os.path.exists(restoreToPath + path):
                    os.makedirs(restoreToPath + path)
                    
                # Create the file with its contents to this directory:
                shutil.copy2(MY_OBJECTS + os.sep + fileList[key], restoreToPath + path + os.sep + file)

            myLogger.logger.info("\tArchive restore completed.")
    except Exception as er:
        myLogger.logger.error("\tException running restoreArchiveToDirectory routine.")
        myLogger.logger.error("\t%s", er)

def restoreArchive(dirname):
    try:
        myLogger.logger.info("\tRestoring archive to - %s", os.curdir + os.sep + dirname)
        
        if len(dirname) > 0:                
            if os.path.exists(os.curdir + os.sep + dirname):
                # Path to restore to exists:
                myLogger.logger.info("\tDirectory to restore to exists - %s", os.curdir + os.sep + dirname)
            else:
                # Create the directory to restore to:
                os.mkdir(os.curdir + os.sep + dirname)
                myLogger.logger.info("\tDirectory to restore to created - %s", os.curdir + os.sep + dirname)
          
            restoreArchiveToDirectory(os.curdir + os.sep + dirname)
        else:
            myLogger.logger.error("\tDirectory name to restore to is invalid/empty.")                
    except Exception as er:
        myLogger.logger.error("\tException running restoreArchive routine.")
        myLogger.logger.error("\t%s", er)

def routineRestore(dirname):
    myLogger.logger.info("\trestore:")
    
    if (myInit.archiveLocationsValid()):
        myLogger.logger.info("\tArchive locations are valid.")
        
        # Continue with restore routine:
        restoreArchive(dirname)
    else:
        myLogger.logger.error("\tArchive locations are not valid.")
