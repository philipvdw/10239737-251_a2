# Import the command line arguments module:
import sys

# Import the logger module:
import myLogger

# Import the routine modules:
import myInit
import myStore
import myTest
import myList
import myGet
import myRestore

def callBackupRoutine(args):
    myLogger.logger.info("\tStarting routine - %s", args[1])
    
    if args[1] == "init":
        myInit.routineInit()         
    elif args[1] == "store":
        # Check to see if a directory is supplied
        # as an argument for store:
        if len(args) > 2:
            myStore.routineStore(args[2])         
        else:
            myLogger.logger.info("\tNo directory path to archive is supplied.")
    elif args[1] == "test":
        myTest.routineTest()         
    elif args[1] == "list":
        pattern = ""
        if len(args) > 2:
            pattern = args[2]
                     
        myList.routineList(pattern)         
    elif args[1] == "get":
        # Check to see if a full filename is supplied
        # to restore:
        if len(args) > 2:
            myGet.routineGet(args[2])         
        else:
            myLogger.logger.info("\tNo full filename is supplied to restore.")
    elif args[1] == "restore":
        # Check to see if a directory is supplied
        # to restore:
        if len(args) > 2:
            myRestore.routineRestore(args[2])         
        else:
            myLogger.logger.info("\tNo directory name to restore to is supplied.")                 
    else:
        myLogger.logger.error("\tStarting routine is invalid.")
        myLogger.logger.error("\tValid arguments are: init, store, test, list, get, restore")
    
# Main program starts:
myLogger.logger.info("Backup program started.")
myLogger.logger.info("---------")
myLogger.logger.info("- BEGIN- ")
myLogger.logger.info("---------")

if len(sys.argv) < 2:
    myLogger.logger.error("\tNo arguments supplied.")
else:
    myLogger.logger.info("\tStartup arguments:")
    for arg in sys.argv:
        myLogger.logger.info('\t\t' + arg)
    callBackupRoutine(sys.argv)

myLogger.logger.info("-------")
myLogger.logger.info("- END -")
myLogger.logger.info("-------")
