# Import operating system module:
import os

# Import high level file operations module:
import shutil

# Import the logger module:
import myLogger

# Import the archive logger:
import myArchiveLogger

# Import other init routine module:
import myInit

# Import the hash library module:
import hashlib

# Define the archive location:
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

# The clear archive method was only a test.
# We don not use it as such.
def clearArchive():
    try:
        myLogger.logger.info("\tClearing current archive:")
        
        entries = os.listdir(MY_OBJECTS)
        
        # Delete each file in the archive:
        for file in entries:
            if file != "index.txt":
                myLogger.logger.info("\t\tDeleting file - %s", MY_OBJECTS + os.sep + file)
                os.remove(MY_OBJECTS + os.sep + file)        
    
        myLogger.logger.info("\tCurrent archive cleared.")
        
        return True;
    except Exception as er:
        myLogger.logger.error("\tException running clearArchive routine.")
        myLogger.logger.error("\t%s", er)
        return False;
        
def backupSelectedDirectoryPath(dirPath):
    try:
        myLogger.logger.info("\tBacking up selected directory path:")
        myArchiveLogger.writeArchiveLog("Store - Directory " + dirPath + " added to the store.")
    
        # Load the contents of the index.txt file back
        # into the dictionary:
        fileList = {}
        if os.path.isfile(MY_INDEX_FILE):
            indexFile = open(MY_INDEX_FILE,'r')
            contents = indexFile.read()
            if len(contents) > 0:
                fileList = eval(contents)
                
            indexFile.close()
        
        # Recursively walk through the directory path to archive
        # and add the files to the archive based on each file's
        # contents:
        for root, dir, files in os.walk(dirPath):
            for file in files:
                myLogger.logger.info("\t\tBacking up file - %s", file)
                
                # Calculate the sha1 hash for this file:
                BLOCKSIZE = 65536
                hasher = hashlib.sha1()
                with open(root + os.sep + file, 'rb') as afile:
                    buf = afile.read(BLOCKSIZE)
                    while len(buf) > 0:
                        hasher.update(buf)
                        buf = afile.read(BLOCKSIZE)
                sha1Hash = hasher.hexdigest()
                
                # Only copy this contents if this contents is not in
                # the archive directory:
                if sha1Hash not in fileList.values():                
                    shutil.copy2(root + os.sep + file, MY_OBJECTS + os.sep + sha1Hash)
                    myArchiveLogger.writeArchiveLog(("File added to the archive - " + root + os.sep + file))
                
                # Add current file to dictionary:
                fileList[root + os.sep + file] = sha1Hash
    
        # Open the index.txt file:
        indexFile = open(MY_INDEX_FILE, 'w')
        
        # Write the fileList dictionary to the index file:
        indexFile.write(str(fileList))
        
        # Close the file handler:
        indexFile.close()
            
        myLogger.logger.info("\tSelected directory path backed up.")
    except Exception as er:
        myLogger.logger.error("\tException running backupSelectedDirectoryPath routine.")
        myLogger.logger.error("\t%s", er)

def routineStore(dirPath):
    myLogger.logger.info("\tstore:")
    
    if (myInit.archiveLocationsValid()):
        myLogger.logger.info("\tArchive locations are valid.")
        
        # Continue with store routine:
        
        # Check to see if the directory path to archive is valid:
        if os.path.exists(dirPath) and os.path.isdir(dirPath):
            myLogger.logger.info("\tThe directory path to archive is valid:")
            myLogger.logger.info("\t %s", dirPath)
            
            # Clear the current archive:
            # if clearArchive():            
            # Backup selected directory path:
            backupSelectedDirectoryPath(dirPath)
        else:
            myLogger.logger.info("\tThe directory path to archive is not valid.")
            myLogger.logger.info("\t %s", dirPath)
    else:
        myLogger.logger.error("\tArchive locations are not valid.")
