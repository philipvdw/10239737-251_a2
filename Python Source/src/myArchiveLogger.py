# Import operating system module:
import os

# Import other init routine module:
import myInit

# Import the logger module:
import myLogger

import datetime

# Define the archive location:
MY_ARCHIVE = os.path.expanduser("~/Desktop/myArchive")
ARCHIVE_LOG = "archive.log"
        
def writeArchiveLog(message):
    try:
        if (myInit.archiveLocationsValid()):
            log = open(MY_ARCHIVE + os.sep + ARCHIVE_LOG, "a")
            
            log.write("\n" + str(datetime.datetime.now()) + " - " + message)
            
            log.close()
    except Exception as er:
        myLogger.logger.error("\tException running writeArchiveLog routine.")
        myLogger.logger.error("\t%s", er)
