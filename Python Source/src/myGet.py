# Import the logger module:
import myLogger

# Import other init routine module:
import myInit

# Import operating system module:
import os

# Import high level file operations module:
import shutil

# Define the archive location:
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

def restoreFile(filename):
    try:
        myLogger.logger.info("\tRestoring file - %s", filename)
        
        if len(filename) > 0:
            # Load the contents of the index.txt file back
            # into the dictionary:
            fileList = {}
            if os.path.isfile(MY_INDEX_FILE):
                indexFile = open(MY_INDEX_FILE,'r')
                contents = indexFile.read()
                if len(contents) > 0:
                    fileList = eval(contents)
                
                indexFile.close()
                
            if filename in fileList:
                shutil.copy2(MY_OBJECTS + os.sep + fileList[filename], os.path.curdir + os.sep + os.path.basename(filename))
                myLogger.logger.info("\tFile restored - %s", filename)
            else:
                myLogger.logger.error("\tFilename to restore is not in the archive - %s", filename)                                
        else:
            myLogger.logger.error("\tFilename to restore is invalid/empty.")                
    except Exception as er:
        myLogger.logger.error("\tException running restoreFile routine.")
        myLogger.logger.error("\t%s", er)

def routineGet(filename):
    myLogger.logger.info("\tget:")
    
    if (myInit.archiveLocationsValid()):
        myLogger.logger.info("\tArchive locations are valid.")
        
        # Continue with get routine:
        restoreFile(filename)
    else:
        myLogger.logger.error("\tArchive locations are not valid.")
