# Import the logger module:
import myLogger

# Import other init routine module:
import myInit

# Import operating system module:
import os

# Define the archive location:
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

def listPatternMatch(pattern):
    try:
        myLogger.logger.info("\tList objects matching pattern - %s" , pattern)
        
        # Load the contents of the index.txt file back
        # into the dictionary:
        fileList = {}
        if os.path.isfile(MY_INDEX_FILE):
            indexFile = open(MY_INDEX_FILE,'r')
            contents = indexFile.read()
            if len(contents) > 0:
                fileList = eval(contents)

            indexFile.close()

        # List the file paths containing the text in
        # the pattern:
        for key in fileList:
            if len(pattern) > 0 and pattern in key:
                print("Pattern - %s", key)                
            if len(pattern) == 0:
                # Print each file path in the archive if pattern is empty:
                print(key)        
    except Exception as er:
        myLogger.logger.error("\tException running listPatternMatch routine.")
        myLogger.logger.error("\t%s", er)
    
def routineList(pattern):
    myLogger.logger.info("\tlist:")
    
    if (myInit.archiveLocationsValid()):
        myLogger.logger.info("\tArchive locations are valid.")
        
        # Continue with list routine:
        listPatternMatch(pattern)
    else:
        myLogger.logger.error("\tArchive locations are not valid.")
