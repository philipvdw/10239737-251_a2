# Import the logger module:
import myLogger

# Import other init routine module:
import myInit

# Import operating system module:
import os

# Import high level file operations module:
import shutil

# Define the archive location:
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

def testObjectsExist():
    try:
        myLogger.logger.info("\tChecking that objects exists in archive.")
        
        # Load the contents of the index.txt file back
        # into the dictionary:
        fileList = {}
        if os.path.isfile(MY_INDEX_FILE):
            indexFile = open(MY_INDEX_FILE,'r')
            contents = indexFile.read()
            if len(contents) > 0:
                fileList = eval(contents)

            indexFile.close()
            
        # Check to see that each key - value exists as an object (file)
        # in the objects directory of the archive:
        countInvalid = 0
        for key in fileList:                
            if not os.path.isfile(MY_OBJECTS + os.sep + fileList[key]):
                myLogger.logger.info("\tFile %s contents does not exist in the archive.", key)
                countInvalid = countInvalid + 1
                
        if countInvalid == 0:
            print("All files exist in the archive.")
        else:
            print("Number of files not in archive - %d" % (countInvalid))
    except Exception as er:
        myLogger.logger.error("\tException running testObjectsExist routine.")
        myLogger.logger.error("\t%s", er)

def routineTest():
    myLogger.logger.info("\ttest:")
    
    if (myInit.archiveLocationsValid()):
        myLogger.logger.info("\tArchive locations are valid.")
        
        # Continue with test routine:
        testObjectsExist()
    else:
        myLogger.logger.error("\tArchive locations are not valid.")
