# Import the logger module:
import myLogger

# Import operating system module:
import os

# Define the archive location:
MY_ARCHIVE = os.path.expanduser("~/Desktop/myArchive")
MY_OBJECTS = os.path.expanduser("~/Desktop/myArchive/objects")
MY_INDEX_FILE = os.path.expanduser("~/Desktop/myArchive/objects/index.txt")

def archiveLocationsValid():
    if os.path.exists(MY_ARCHIVE) and os.path.isdir(MY_ARCHIVE) and \
       os.path.exists(MY_OBJECTS) and os.path.isdir(MY_OBJECTS) and \
       os.path.isfile(MY_INDEX_FILE):
        return True
    else:
        return False
    
def createObjectsLocation():
        os.mkdir(MY_OBJECTS)
        myLogger.logger.info("\tObjects location %s created.", MY_OBJECTS)

def createIndexFile():
        open(MY_INDEX_FILE, "w+").close()
        myLogger.logger.info("\tIndex file %s created.", MY_INDEX_FILE)
    
def routineInit():
    myLogger.logger.info("\tinit:")
    
    try:
        if os.path.exists(MY_ARCHIVE) and os.path.isdir(MY_ARCHIVE):
            myLogger.logger.info("\tArchive location %s exists.", MY_ARCHIVE)
            
            if os.path.exists(MY_OBJECTS) and os.path.isdir(MY_OBJECTS):
                myLogger.logger.info("\tObjects location %s exists.", MY_OBJECTS)
            else:
                # Create the sub-directory objects:
                createObjectsLocation()
                
            if os.path.isfile(MY_INDEX_FILE):
                myLogger.logger.info("\tIndex file %s exists.", MY_INDEX_FILE)
            else:
                # Create the sub-directory objects:
                createIndexFile()            
        else:
            # Create the archive location:
            os.mkdir(MY_ARCHIVE)
            myLogger.logger.info("\tArchive location %s created.", MY_ARCHIVE)
            
            # Create the sub-directory objects:
            createObjectsLocation()
            
            # Create the empty index file:
            createIndexFile()
    except Exception as er:
        myLogger.logger.error("\tException running init routine:")
        myLogger.logger.error("\t%s", er)
        