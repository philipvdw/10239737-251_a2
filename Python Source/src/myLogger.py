'''
Logger for logging all actions taken by the mybackup module.
The logger is used throughout all the  modules that makes up
this backup program.
'''

# Import the needed modules:
import logging

# Set the logger filename:
PROGRAM_NAME = "mybackup"
LOG_FILENAME = PROGRAM_NAME + ".log"

# Log all errors to the console:
CONSOLE_LOG_LEVEL = logging.ERROR

# Log all information to the log file:
FILE_LOG_LEVEL = logging.INFO

logger = logging.getLogger(PROGRAM_NAME)
logger.setLevel(logging.DEBUG)

# File-based log:
# BEGIN
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# The log file handler will be a continuous single log file:
logfileHandler = logging.FileHandler(LOG_FILENAME)

logfileHandler.setLevel(FILE_LOG_LEVEL)
logfileHandler.setFormatter(formatter)
logger.addHandler(logfileHandler)
# END

# Console-based log:
# BEGIN
formatter = logging.Formatter('%(message)s')

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(CONSOLE_LOG_LEVEL)
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)
# END
